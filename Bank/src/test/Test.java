package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import models.Account;
import models.Bank;
import models.Person;
import models.SpendingAccount;

public class Test {

	@org.junit.Test
	public void test1() {
		Bank banca = new Bank();
		Person p = new Person(2, "Biris Alexandra", "Banat 19/7");
		banca.addPerson(p);
		
		for (HashMap.Entry<Person, HashSet<Account>> entry : banca.getBank().entrySet()) {
		    Person key = entry.getKey();
		    assertEquals(key, p);
		}
	}
	
	@org.junit.Test
	public void test2() {
		Bank banca = new Bank();
		Person p = new Person(2, "Biris Alexandra", "Banat 19/7");
		banca.addPerson(p);
		banca.removePerson(p);
		
		for (HashMap.Entry<Person, HashSet<Account>> entry : banca.getBank().entrySet()) {
		    Person key = entry.getKey();
		    assertNotEquals(key, p);
		}
	}
	
	@org.junit.Test
	public void test3() {
		Bank banca = new Bank();
		Person p = new Person(2, "Biris Alexa", "Banat 19/7");
		Account a = new SpendingAccount(12, 200);
		banca.addPerson(p);
		banca.addAccount(p, a);
		
		for (HashMap.Entry<Person, HashSet<Account>> entry : banca.getBank().entrySet()) {
		    Person key = entry.getKey();
		    if (key.equals(p)) {
		    	HashSet<Account> accounts = entry.getValue();
				Iterator<Account> it= accounts.iterator();
				while (it.hasNext()) {
					Account account = it.next();
					assertEquals(a, account);
				}
		    }
		}
	}
	
	@org.junit.Test
	public void test4() {
		Bank banca = new Bank();
		Person p = new Person(2, "Biris Alexa", "Banat 19/7");
		Account a = new SpendingAccount(12, 200);
		banca.addPerson(p);
		banca.addAccount(p, a);
		banca.removeAccount(p, a);
		
		for (HashMap.Entry<Person, HashSet<Account>> entry : banca.getBank().entrySet()) {
		    Person key = entry.getKey();
		    if (key.equals(p)) {
		    	HashSet<Account> accounts = entry.getValue();
				Iterator<Account> it= accounts.iterator();
				while (it.hasNext()) {
					Account account = it.next();
					assertNotEquals(a, account);
				}
		    }
		}
	}
}
