package models;

import java.io.Serializable;

import javax.swing.JOptionPane;

public class SpendingAccount extends Account implements Serializable {
	private String s = "Spending";
	
	public SpendingAccount(int id, float money) {
		super(id, money);
	}

	@Override
	public void deposit(float dep) {
		this.setMoney(this.getMoney() + dep);
	}

	@Override
	public void withdraw(float sum) {
		if (this.getMoney() >= sum)
			this.setMoney(this.getMoney() - sum);
		else
			JOptionPane.showMessageDialog(null, "Not enough fonds!");
		
	}
}
