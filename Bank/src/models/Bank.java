package models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class Bank implements BankProc{
	public HashMap<Person, HashSet<Account>> bank;
	
	
	public Bank() {
		bank = new HashMap<Person, HashSet<Account>>();
	}

	public HashMap<Person, HashSet<Account>> getBank() {
		return bank;
	}

	public void setBank(HashMap<Person, HashSet<Account>> bank) {
		this.bank = bank;
	}
	
	public boolean isWellFormed() {
		if (bank == null)
			return false;
		for (HashMap.Entry<Person, HashSet<Account>> entry : bank.entrySet()) {
		    if (entry.getKey() == null)
		    	return false;
		    if(entry.getValue() == null)
		    	return false;
		}
		return true;
	}

	@Override
	public void addPerson(Person p) {
		assert p != null : "Person is null!";
		assert isWellFormed();
		int aux = bank.size();
		bank.put(p, new HashSet<Account>()); 
		assert bank.size() == aux + 1 : "Could not add person";
	}

	@Override
	public void removePerson(Person p) {
		assert p != null : "Person is null!";
		assert isWellFormed();
		int aux = bank.size();
		bank.remove(p);
		assert bank.size() == aux - 1 : "Could not remove person";
	}

	@Override
	public void addAccount(Person p, Account a) {
		assert p != null : "Person is null!";
		assert a != null : "Account is null";
		assert isWellFormed();
		a.addObserver(p);
		HashSet<Account> s = bank.get(p);
		int aux = bank.get(p).size();
		s.add(a);
		bank.put(p, s);
		assert bank.get(p).size() == aux + 1 : "Could not add account";
	}

	@Override
	public void removeAccount(Person p, Account a) {
		assert p != null : "Person is null!";
		assert a != null: "Account is null";
		assert isWellFormed();
		HashSet<Account> s = bank.get(p);
		int aux = bank.get(p).size();
		Iterator<Account> i = s.iterator();
		while (i.hasNext()) 
			if (i.next().getId() == a.getId())
				i.remove();
		assert bank.get(p).size() == aux - 1 : "Could not remove account";
	}
	
	public Person findPerson(int id) {
		for (HashMap.Entry<Person, HashSet<Account>> entry: bank.entrySet()) {
			Person key = entry.getKey();
			if (key.getId() == id)
				return new Person(key.getId(), key.getName(), key.getAddress());
		}
		return null;
	}
	
	public Person findPersonByName(String name) {
		for (HashMap.Entry<Person, HashSet<Account>> entry: bank.entrySet()) {
			Person key = entry.getKey();
			if (key.getName() == name)
				return new Person(key.getId(), key.getName(), key.getAddress());
		}
		return null;
	}
	
	public Account findAccountByIban(int Iban) {
		for (HashMap.Entry<Person, HashSet<Account>> entry : bank.entrySet()) {
			HashSet<Account> accounts = entry.getValue();
		    Iterator<Account> it= accounts.iterator();
		    while (it.hasNext()) {
		    	Account account = it.next();
		    	if (account.getId() == Iban) 
		    		return account;
		    }	    
        }
		return null;
	}
}
