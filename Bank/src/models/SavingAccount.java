package models;

import java.io.Serializable;

import javax.swing.JOptionPane;

public class SavingAccount extends Account implements Serializable{
	private String s = "Saving";
	private int months;
	private boolean availability;

	public SavingAccount(int id, float money, int months) {
		super(id, money);
		this.availability = true;
		this.months = months;
	}
	
	public int getMonths() {
		return months;
	}

	public void setMonths(int months) {
		this.months = months;
	}

	@Override
	public void deposit(float amount) {
		if (this.availability == true)
			this.setMoney(this.getMoney() + amount + this.getMoney() * months * 0.01f);
		else
			JOptionPane.showMessageDialog(null, "Account can not be used anymore!");
	}

	@Override
	public void withdraw(float sum) {
		if (this.availability == true) {
			if (this.getMoney() >= sum) {
				this.setMoney(this.getMoney() - sum);
				this.availability = false;
			}
			else {
				JOptionPane.showMessageDialog(null, "Not enough fonds!");
			}
		}
		else 
			JOptionPane.showMessageDialog(null, "Account can not be used anymore!");
	}
}
