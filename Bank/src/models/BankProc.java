package models;

public interface BankProc {
	
	/**
	 * @pre p != null
	 * @post getSize() == getSize()@pre + 1
	 * @param p
	 */
	public void addPerson(Person p);
	
	/**
	 * @pre p != null
	 * @post getSize() == getSize()@pre - 1
	 * @param p
	 */
	public void removePerson(Person p);
	
	/**
	 * @pre p != null && a != null
	 * @post map.get(p).size == map.get(p).size@pre + 1
	 * @param p
	 * @param a
	 */
	public void addAccount(Person p, Account a);
	
	/**
	 * @pre p != null && a != null
	 * @post map.get(p).size == map.get(p).size@pre - 1;
	 * @param p
	 * @param a
	 */
	public void removeAccount(Person p, Account a);
}
