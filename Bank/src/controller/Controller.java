package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import models.Account;
import models.Bank;
import models.Person;
import models.SavingAccount;
import models.SpendingAccount;
import view.GUI;

public class Controller {
	private GUI g;
	private Bank bank;

	public Controller(GUI g) {
		this.g = g;
		this.bank = new Bank();
		deserialize();
		setComboBox();
		this.g.addNewClientListener(new NewClient());
		this.g.addViewClientsListener(new ViewClients());
		this.g.addDeleteClientListener(new DeleteClient());
		this.g.addEditClientListener(new EditClient());
		this.g.addViewAllListener(new ViewAccounts());
		this.g.addAddNewListener(new NewAccount());
		this.g.addDeleteListener(new DeleteAccount());
		this.g.addEditListener(new EditAccount());
		this.g.addDepositListener(new Deposit());
		this.g.addWithdrawListener(new Withdraw());
		g.getJText().setEnabled(false);
	}
	
	public void serialize() {
		try {
			FileOutputStream fileOut = new FileOutputStream("bankf.ser");
		    ObjectOutputStream out = new ObjectOutputStream(fileOut);
		    out.writeObject(bank.getBank());
		    out.close();
		    fileOut.close();
		    System.out.printf("Serialized data is saved in bankfile.ser\n");
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void deserialize() {
		 try {
		   	FileInputStream fileIn = new FileInputStream("bankf.ser");
	        ObjectInputStream in = new ObjectInputStream(fileIn);
	        bank.setBank((HashMap<Person, HashSet<Account>>) in.readObject());
	        in.close();
		    fileIn.close();
		 } catch(IOException i) {
	         i.printStackTrace();
	     } catch(ClassNotFoundException c) {
	    	 System.out.println("Person class not found\n");
		     c.printStackTrace();
	     }
	}
	
	public class NewClient implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Person p = new Person(g.getPersonId(), g.getTextFieldName(), g.getTextFieldAddress());
			bank.addPerson(p);
			serialize();
		}
	}
	
	public class ViewClients implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			g.updatezTable(getData());
			serialize();
		}
	}
	
	private Object[][] getData() {
		Object[][] date = new Object[100][3];
		int i = 0;
		for (HashMap.Entry<Person, HashSet<Account>> entry: bank.getBank().entrySet()) {
			Person key = entry.getKey();
			date[i][0] = key.getId();
			date[i][1] = key.getName();
			date[i][2] = key.getAddress();
			i++;
		}
		return date;
	}
	
	public class DeleteClient implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Person p = new Person(g.getPersonId(), g.getTextFieldName(), g.getTextFieldAddress());
			HashMap<Person, HashSet<Account>> bib = bank.getBank();
			if(bib.containsKey(p)) {
				bank.removePerson(p);
				System.out.println("Person was deleted");
			}
			else
				System.out.println("The person does not exist!");
			serialize();
		}
	}
	
	public class EditClient implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Person p1 = bank.findPerson(g.getPersonId());
			bank.removePerson(p1);
			Person p2 = new Person(g.getPersonId(), g.getTextFieldName(), g.getTextFieldAddress());
			bank.addPerson(p2);
			serialize();
		}
	}
	
	public void setComboBox() {
		for (HashMap.Entry<Person, HashSet<Account>> entry: bank.getBank().entrySet()) {
			Person key = entry.getKey();
			g.getComboBoxPerson().addItem(key.getName());
		}
	}
	
	private Object[][] getData2() {
		Object[][] date = new Object[100][4];
		int i = 0;
		for (HashMap.Entry<Person, HashSet<Account>> entry : bank.getBank().entrySet()) {
		    Person key = entry.getKey();
		    HashSet<Account> accounts = entry.getValue();
		    Iterator<Account> it= accounts.iterator();
		    while (it.hasNext()) {
		    	Account account = it.next();
		    	date[i][0] = key.getName();
			    date[i][1] = account.getId();
			    if (account instanceof SavingAccount)
			    	date[i][2] = "Saving";
			    else
			    	date[i][2] = "Spending";
			    date[i][3] = account.getMoney();
			    i++;
		    }	    
        }
		return date;
	}
	
	public class ViewAccounts implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			g.updatezTable2(getData2());
			serialize();
		}
	}
	
	public class NewAccount implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Person p = bank.findPersonByName(g.getComboBoxPerson().getSelectedItem().toString());
			if (g.getComboBoxType().getSelectedItem().equals("Spending")) {
				Account a = new SpendingAccount(g.getTextFieldIban(), 0);
				bank.addAccount(p, a);
			}
			else if (g.getComboBoxType().getSelectedItem().equals("Saving")) {
				Account a = new SavingAccount(g.getTextFieldIban(), g.getTextFieldMonth(), 0);
				bank.addAccount(p, a);
			}
			serialize();
		}
	}
	
	public class DeleteAccount implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Person p = bank.findPersonByName(g.getComboBoxPerson().getSelectedItem().toString());
			Account a;
			if (g.getComboBoxType().getSelectedItem().equals("Spending")) {
				a = new SpendingAccount(g.getTextFieldIban(), g.getTextFieldSum());
			}
			else {
				a = new SavingAccount(g.getTextFieldIban(), g.getTextFieldSum(), 0); 
			}
			bank.removeAccount(p, a);
			serialize();
		}
	}
	
	public class EditAccount implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String name = g.getComboBoxPerson().getSelectedItem().toString();
			for (HashMap.Entry<Person, HashSet<Account>> entry : bank.getBank().entrySet()) {
			    Person key = entry.getKey();
			    if (key.getName().equals(name)) {
			    	HashSet<Account> accounts = entry.getValue();
			    	Iterator<Account> it= accounts.iterator();
			    	while (it.hasNext()) {
			    		Account account = it.next();
			    		if (account.getId() == g.getTextFieldIban()) {
			    			account.setMoney(g.getTextFieldSum());
			    			if (account instanceof SavingAccount)
			    				((SavingAccount) account).setMonths(g.getTextFieldMonth());
			    			serialize();
			    		}
			    	}
			    }	    
	        }
		}
	}
	
	public class Deposit implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Account a = bank.findAccountByIban(g.getTextFieldIban());
			a.deposit(g.getTextFieldSum());
		}
	}
	
	public class Withdraw implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Account a = bank.findAccountByIban(g.getTextFieldIban());
			a.withdraw(g.getTextFieldSum());
		}
	}
}
