package view;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JPanel panelMenu = new JPanel();
	private JPanel panelClient = new JPanel();
	private JPanel panelAccount = new JPanel();
	private JTable table = new JTable();
	private JScrollPane scrollPane = new JScrollPane();
	private JScrollPane scrollPane_1 = new JScrollPane();
	private JTextField textField = new JTextField();
	private JTextField textField_1 = new JTextField();
	private JTextField textField_2 = new JTextField();
	private JButton btnNewButton = new JButton("View clients");
	private JButton btnAddNewClient = new JButton("New client");
	private JButton btnUpdateClient = new JButton("Update client");
	private JButton btnDeleteClient = new JButton("Delete client");
	private JTextField textFieldSum = new JTextField();
	private JTextField textFieldMonth = new JTextField();
	private JTable table_1 = new JTable();
	private String[] type = {"Spending", "Saving"};
	private JComboBox comboBoxType = new JComboBox(type);
	private JComboBox comboBoxPerson = new JComboBox();
	private JButton btnWithdraw = new JButton("Withdraw");
	private JButton btnDeposit = new JButton("Deposit");
	private JButton btnViewAll = new JButton("View all ");
	private JButton btnAddNew = new JButton("Add new");
	private JButton btnDelete = new JButton("Delete");
	private JButton btnEdit = new JButton("Edit");
	private JLabel lblIban = new JLabel("IBAN");
	private JTextField textFieldIban = new JTextField();
	
	/**
	 * Create the frame.
	 */
	public GUI() {
		textFieldIban.setBounds(318, 228, 155, 20);
		textFieldIban.setColumns(10);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 574, 363);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		contentPane.add(panelMenu, "name_40249760706210");
		panelMenu.setLayout(null);
		
		JButton btnClient = new JButton("Client");
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelMenu.setVisible(false);
				panelClient.setVisible(true);
			}
		});
		btnClient.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnClient.setBounds(209, 111, 134, 34);
		panelMenu.add(btnClient);
		
		JButton btnAccount = new JButton("Account");
		btnAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelMenu.setVisible(false);
				panelAccount.setVisible(true);
			}
		});
		btnAccount.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnAccount.setBounds(209, 173, 134, 34);
		panelMenu.add(btnAccount);
		
		JLabel lblChooseAnOperation = new JLabel("Choose an operation: ");
		lblChooseAnOperation.setBounds(224, 76, 134, 24);
		panelMenu.add(lblChooseAnOperation);
		
		contentPane.add(panelClient, "name_40251564173971");
		panelClient.setLayout(null);
		
		scrollPane.setBounds(31, 11, 313, 211);
		panelClient.add(scrollPane);
		
		btnNewButton.setBounds(23, 246, 110, 23);
		panelClient.add(btnNewButton);
		
		btnAddNewClient.setBounds(154, 246, 110, 23);
		panelClient.add(btnAddNewClient);
		
		btnUpdateClient.setBounds(289, 246, 110, 23);
		panelClient.add(btnUpdateClient);
		
		btnDeleteClient.setBounds(416, 246, 110, 23);
		panelClient.add(btnDeleteClient);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelClient.setVisible(false);
				panelMenu.setVisible(true);
			}
		});
		btnBack.setBounds(232, 280, 89, 23);
		panelClient.add(btnBack);
		
		textField.setBounds(440, 76, 86, 20);
		panelClient.add(textField);
		textField.setColumns(10);
		
		textField_1.setBounds(440, 116, 86, 20);
		panelClient.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2.setBounds(440, 153, 86, 20);
		panelClient.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(405, 79, 46, 14);
		panelClient.add(lblId);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(394, 119, 46, 14);
		panelClient.add(lblName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(383, 155, 58, 17);
		panelClient.add(lblAddress);
		
		contentPane.add(panelAccount, "name_40253321372136");
		panelAccount.setLayout(null);
		
		JLabel lblTypeOfAccount = new JLabel("Type of account:");
		lblTypeOfAccount.setBounds(45, 60, 103, 21);
		panelAccount.add(lblTypeOfAccount);
		
		comboBoxType.setBounds(26, 92, 122, 20);
		panelAccount.add(comboBoxType);
		comboBoxType.setEditable(true);
		comboBoxType.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED)
					if (arg0.getItem().equals("Spending"))
						textFieldMonth.setEnabled(false);
					else 
						textFieldMonth.setEnabled(true);}
		});
		
		JLabel lblNewLabel = new JLabel("Sum:");
		lblNewLabel.setBounds(26, 123, 82, 21);
		panelAccount.add(lblNewLabel);
		
		textFieldSum.setBounds(66, 123, 82, 20);
		panelAccount.add(textFieldSum);
		textFieldSum.setColumns(10);
		
		comboBoxPerson.setBounds(26, 36, 122, 20);
		panelAccount.add(comboBoxPerson);
		comboBoxPerson.setEditable(true);
		
		JLabel lblPerson = new JLabel("Person: ");
		lblPerson.setBounds(62, 11, 57, 21);
		panelAccount.add(lblPerson);
		
		btnDeposit.setBounds(48, 194, 89, 23);
		panelAccount.add(btnDeposit);
		
		btnWithdraw.setBounds(48, 231, 89, 23);
		panelAccount.add(btnWithdraw);
		
		JLabel lblMonths = new JLabel("Months:");
		lblMonths.setBounds(26, 155, 46, 14);
		panelAccount.add(lblMonths);
		
		textFieldMonth.setBounds(77, 152, 70, 20);
		panelAccount.add(textFieldMonth);
		textFieldMonth.setColumns(10);
		
		JButton btnBack_1 = new JButton("Back");
		btnBack_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelAccount.setVisible(false);
				panelMenu.setVisible(true);
			}
		});
		btnBack_1.setBounds(45, 265, 92, 23);
		panelAccount.add(btnBack_1);
		
		scrollPane_1.setBounds(200, 14, 319, 186);
		panelAccount.add(scrollPane_1);
		btnViewAll.setBounds(157, 265, 89, 23);
		
		panelAccount.add(btnViewAll);
		btnAddNew.setBounds(256, 265, 89, 23);
		
		panelAccount.add(btnAddNew);
		btnDelete.setBounds(449, 265, 89, 23);
		
		panelAccount.add(btnDelete);
		btnEdit.setBounds(355, 265, 89, 23);
		
		panelAccount.add(btnEdit);
		lblIban.setBounds(273, 231, 46, 14);
		
		panelAccount.add(lblIban);
		
		panelAccount.add(textFieldIban);
			
	}
	public void updatezTable2(Object[][] lista) {
		table_1 = new JTable(lista, new Object[]{"Person", "IBAN", "Type", "Balance"});
		scrollPane_1.setViewportView(table_1);
		table_1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				textFieldIban.setText(table_1.getModel().getValueAt(table_1.getSelectedRow(), 1).toString());
				textFieldSum.setText(table_1.getModel().getValueAt(table_1.getSelectedRow(), 3).toString());
				comboBoxType.getEditor().setItem(table_1.getModel().getValueAt(table_1.getSelectedRow(), 2).toString());
				comboBoxPerson.getEditor().setItem(table_1.getModel().getValueAt(table_1.getSelectedRow(), 0).toString());
			}
		});
	}
	
	public void updatezTable(Object[][] lista) {
		table = new JTable(lista, new Object[]{"ID", "Name", "Address"});
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				textField.setText(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());
				textField_1.setText(table.getModel().getValueAt(table.getSelectedRow(), 1).toString());
				textField_2.setText(table.getModel().getValueAt(table.getSelectedRow(), 2).toString());	
			}	
		});
		scrollPane.setViewportView(table);}
	//Person panel
	public int getPersonId() {
		return Integer.parseInt(textField.getText());}
	public String getTextFieldName() {
		return textField_1.getText();}
	public String getTextFieldAddress() {
		return textField_2.getText();}
	public void setTextFieldID(String id) {
		textField.setText(id);}
	public void setTextFieldName(String name) {
		textField_1.setText(name);}
	public void setTextFieldAddress(String address) {
		textField_2.setText(address);}
	
	public void addNewClientListener(ActionListener a) {
		btnAddNewClient.addActionListener(a);}
	public void addViewClientsListener(ActionListener a) {
		btnNewButton.addActionListener(a);}
	public void addEditClientListener(ActionListener a) {
		btnUpdateClient.addActionListener(a);}
	public void addDeleteClientListener(ActionListener a) {
		btnDeleteClient.addActionListener(a);}
	//Account panel
	public Float getTextFieldSum() {
		return Float.parseFloat(textFieldSum.getText());}
	public int getTextFieldMonth() {
		return Integer.parseInt(textFieldMonth.getText());}
	public JTextField getJText() {
		return this.textFieldMonth;}
	public int getTextFieldIban() {
		return Integer.parseInt(textFieldIban.getText());}
	
	public void addDepositListener(ActionListener a) {
		btnDeposit.addActionListener(a);}
	public void addWithdrawListener(ActionListener a) {
		btnWithdraw.addActionListener(a);}
	public void addViewAllListener(ActionListener a) {
		btnViewAll.addActionListener(a);}
	public void addAddNewListener(ActionListener a) {
		btnAddNew.addActionListener(a);}
	public void addDeleteListener(ActionListener a) {
		btnDelete.addActionListener(a);}
	public void addEditListener(ActionListener a) {
		btnEdit.addActionListener(a);}
	
	public JComboBox<String> getComboBoxType() {
		return comboBoxType;}
	public JComboBox<String> getComboBoxPerson() {
		return comboBoxPerson;}
}
